<?php
class LinkedModel extends CI_Model {
	public function __construct()  {
		parent::__construct();
	}
	public function getVinculatesData()  {
        $query = $this->db->from('Vinculados')->get();
        return $query->result();
	}
}