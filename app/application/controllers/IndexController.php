<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class IndexController extends REST_Controller {

    public function __construct(){
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
        $this->load->model('LinkedModel');
    }

	public function getdata_get() {
		$response = $this->LinkedModel->getVinculatesData();
		
        return $this->response(array(
            'success'=>true, 
            'code' => REST_Controller::HTTP_OK,
            'data' => $response
        ), REST_Controller::HTTP_OK);
	}

	
	public function generate_excel_get(){
		$response = $this->LinkedModel->getVinculatesData();

		if(count($response) > 0){
			//Cargamos la librería de excel.
			$this->load->library('excel');
			$this->excel->setActiveSheetIndex(0);
			$this->excel->getActiveSheet()->setTitle('Vinculados');
			//Contador de filas
			$contador = 1;
			//Le aplicamos ancho las columnas.
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AR')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AS')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AT')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AU')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AV')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AW')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AX')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AY')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('AZ')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('BA')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('BB')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('BC')->setWidth(20);

			// //Le aplicamos negrita a los títulos de la cabecera.
			$this->excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("G{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("H{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("I{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("J{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("K{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("L{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("M{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("N{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("O{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("P{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("Q{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("R{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("S{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("T{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("U{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("V{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("W{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("X{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("Y{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("Z{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AA{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AB{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AC{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AD{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AE{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AF{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AG{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AH{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AI{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AJ{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AK{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AL{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AM{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AN{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AO{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AP{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AQ{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AR{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AS{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AT{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AU{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AV{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AW{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AX{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AY{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("AZ{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("BA{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("BB{$contador}")->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle("BC{$contador}")->getFont()->setBold(true);


			$body = 1; 
			//Definimos los títulos de la cabecera.
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0,$body, 'UltimaModificacion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1,$body, 'UsuarioUltimaModificacion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2,$body, 'VinculadoID');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3,$body, 'FechaVinculacion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4,$body, 'Nombres');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5,$body, 'Apellidos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6,$body, 'Nacionalidad');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7,$body, 'OtraNacionalidad');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8,$body, 'TipoDocumento');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9,$body, 'OtroDocumento');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10,$body, 'NumeroID');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11,$body, 'FechadeExpedicion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(12,$body, 'CiudadExpedicion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(13,$body, 'PaisExpedicion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(14,$body, 'FechaNacimiento');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(15,$body, 'CiudadNacimiento');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(16,$body, 'PaisNacimiento');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(17,$body, 'Genero');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(18,$body, 'EstadoCivil');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(19,$body, 'DireccionResidencia');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(20,$body, 'TelefonoResidencia');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(21,$body, 'Celular');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(22,$body, 'CiudadResidencia');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(23,$body, 'DepartamentoResidencia');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(24,$body, 'CorreoElectronico');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(25,$body, 'Ocupacion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(26,$body, 'DireccionEmpresa');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(27,$body, 'TelefonoEmpresa');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(28,$body, 'CiudadEmpresa');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(29,$body, 'DepartamentoEmpresa');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(30,$body, 'NombreEmpresa');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(31,$body, 'Cargo');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(32,$body, 'CIIU');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(33,$body, 'Activos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(34,$body, 'Pasivos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(35,$body, 'IngresosMensuales');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(36,$body, 'EgresosMensuales');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(37,$body, 'OtrosIngresos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(38,$body, 'OtrosEgresos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(39,$body, 'DeclaraRenta');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(40,$body, 'RazondeIngresos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(41,$body, 'Producto');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(42,$body, 'NivelEstudios');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(43,$body, 'Beneficiario');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(44,$body, 'TipoDocumentoBeneficiario');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(45,$body, 'NumeroIDBeneficiario');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(46,$body, 'EscolaridadBeneficiario');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(47,$body, 'MontoAhorroMensual');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(48,$body, 'ValorCadaBono');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(49,$body, 'CantidadBonos');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(50,$body, 'PrimaJunio');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(51,$body, 'PrimaDiciembre');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(52,$body, 'DebitoAutomatico');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(53,$body, 'FechadeAfiliacion');
			$this->excel->getActiveSheet()->setCellValueByColumnAndRow(54,$body, 'DuenoOportunidad');

			//Definimos la data del cuerpo.  
			$body = 2;     
			foreach($response as $data){
				//Incrementamos una fila más, para ir a la siguiente.
				$contador++;
				//Informacion de las filas de la consulta.

				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(0,$body, $data->UltimaModificacion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(1,$body, $data->UsuarioUltimaModificacion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(2,$body, $data->VinculadoID);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(3,$body, $data->FechaVinculacion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(4,$body, $data->Nombres);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(5,$body, $data->Apellidos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(6,$body, $data->Nacionalidad);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(7,$body, $data->OtraNacionalidad);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(8,$body, $data->TipoDocumento);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(9,$body, $data->OtroDocumento);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(10,$body, $data->NumeroID);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(11,$body, $data->FechadeExpedicion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(12,$body, $data->CiudadExpedicion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(13,$body, $data->PaisExpedicion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(14,$body, $data->FechaNacimiento);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(15,$body, $data->CiudadNacimiento);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(16,$body, $data->PaisNacimiento);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(17,$body, $data->Genero);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(18,$body, $data->EstadoCivil);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(19,$body, $data->DireccionResidencia);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(20,$body, $data->TelefonoResidencia);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(21,$body, $data->Celular);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(22,$body, $data->CiudadResidencia);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(23,$body, $data->DepartamentoResidencia);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(24,$body, $data->CorreoElectronico);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(25,$body, $data->Ocupacion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(26,$body, $data->DireccionEmpresa);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(27,$body, $data->TelefonoEmpresa);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(28,$body, $data->CiudadEmpresa);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(29,$body, $data->DepartamentoEmpresa);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(30,$body, $data->NombreEmpresa);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(31,$body, $data->Cargo);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(32,$body, $data->CIIU);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(33,$body, $data->Activos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(34,$body, $data->Pasivos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(35,$body, $data->IngresosMensuales);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(36,$body, $data->EgresosMensuales);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(37,$body, $data->OtrosIngresos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(38,$body, $data->OtrosEgresos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(39,$body, $data->DeclaraRenta);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(40,$body, $data->RazondeIngresos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(41,$body, $data->Producto);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(42,$body, $data->NivelEstudios);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(43,$body, $data->Beneficiario);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(44,$body, $data->TipoDocumentoBeneficiario);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(45,$body, $data->NumeroIDBeneficiario);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(46,$body, $data->EscolaridadBeneficiario);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(47,$body, $data->MontoAhorroMensual);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(48,$body, $data->ValorCadaBono);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(49,$body, $data->CantidadBonos);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(50,$body, $data->PrimaJunio);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(51,$body, $data->PrimaDiciembre);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(52,$body, $data->DebitoAutomatico);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(53,$body, $data->FechadeAfiliacion);
				$this->excel->getActiveSheet()->setCellValueByColumnAndRow(54,$body, $data->DuenoOportunidad);

				$body++;
			}

			//Le ponemos un nombre al archivo que se va a generar.
			$archivo = "vinculados_empresa.xls";
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$archivo.'"');
			header('Cache-Control: max-age=0');
			//$objWriter = new PHPExcel_Writer_Excel2007($this->excel);
			print_r($this->excel);
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

			//Hacemos una salida al navegador con el archivo Excel.
			$objWriter->save('php://output');
		}else{
			echo 'No se han encontrado resultados';
			exit;        
		}
	}
}