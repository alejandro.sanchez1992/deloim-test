# Test Fullstack Deloim By Alejandro Sanchez
Docker setup for php-7 on Nginx with mysql and Angular

### Requirements:
docker-compose (and all pre-reqs)

### To run:
1. clone this repo
2. run `docker-compose up`

### View App Frontend:
`http://localhost:4200`

### View App Api:
`http://localhost:8088`