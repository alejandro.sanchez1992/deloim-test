-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `Vinculados`;
CREATE TABLE `Vinculados` (
  `UltimaModificacion` datetime DEFAULT NULL,
  `UsuarioUltimaModificacion` longtext,
  `VinculadoID` int(11) NOT NULL AUTO_INCREMENT,
  `FechaVinculacion` datetime(6) NOT NULL,
  `Nombres` varchar(250) NOT NULL,
  `Apellidos` varchar(250) NOT NULL,
  `Nacionalidad` varchar(250) DEFAULT NULL,
  `OtraNacionalidad` varchar(250) DEFAULT NULL,
  `TipoDocumento` varchar(100) DEFAULT NULL,
  `OtroDocumento` varchar(100) DEFAULT NULL,
  `NumeroID` varchar(100) DEFAULT NULL,
  `FechadeExpedicion` datetime(6) NOT NULL,
  `CiudadExpedicion` varchar(100) DEFAULT NULL,
  `PaisExpedicion` varchar(100) DEFAULT NULL,
  `FechaNacimiento` datetime(6) NOT NULL,
  `CiudadNacimiento` varchar(100) DEFAULT NULL,
  `PaisNacimiento` varchar(100) DEFAULT NULL,
  `Genero` varchar(25) DEFAULT NULL,
  `EstadoCivil` varchar(100) DEFAULT NULL,
  `DireccionResidencia` varchar(250) DEFAULT NULL,
  `TelefonoResidencia` varchar(25) DEFAULT NULL,
  `Celular` varchar(25) DEFAULT NULL,
  `CiudadResidencia` varchar(100) DEFAULT NULL,
  `DepartamentoResidencia` varchar(100) DEFAULT NULL,
  `CorreoElectronico` varchar(200) DEFAULT NULL,
  `Ocupacion` varchar(200) DEFAULT NULL,
  `DireccionEmpresa` varchar(200) DEFAULT NULL,
  `TelefonoEmpresa` varchar(25) DEFAULT NULL,
  `CiudadEmpresa` varchar(100) DEFAULT NULL,
  `DepartamentoEmpresa` varchar(100) DEFAULT NULL,
  `NombreEmpresa` varchar(200) DEFAULT NULL,
  `Cargo` varchar(200) DEFAULT NULL,
  `CIIU` varchar(20) DEFAULT NULL,
  `Activos` bigint(20) NOT NULL,
  `Pasivos` bigint(20) NOT NULL,
  `IngresosMensuales` bigint(20) NOT NULL,
  `EgresosMensuales` bigint(20) NOT NULL,
  `OtrosIngresos` bigint(20) NOT NULL,
  `OtrosEgresos` bigint(20) NOT NULL,
  `DeclaraRenta` bit(1) NOT NULL,
  `RazondeIngresos` varchar(200) DEFAULT NULL,
  `Producto` varchar(200) DEFAULT NULL,
  `NivelEstudios` varchar(200) DEFAULT NULL,
  `Beneficiario` varchar(200) DEFAULT NULL,
  `TipoDocumentoBeneficiario` varchar(100) DEFAULT NULL,
  `NumeroIDBeneficiario` varchar(100) DEFAULT NULL,
  `EscolaridadBeneficiario` varchar(200) DEFAULT NULL,
  `MontoAhorroMensual` bigint(20) NOT NULL,
  `ValorCadaBono` bigint(20) NOT NULL,
  `CantidadBonos` int(11) NOT NULL,
  `PrimaJunio` bigint(20) NOT NULL,
  `PrimaDiciembre` bigint(20) NOT NULL,
  `DebitoAutomatico` bit(1) NOT NULL,
  `FechadeAfiliacion` datetime(6) NOT NULL,
  `DuenoOportunidad` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`VinculadoID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Vinculados` (`UltimaModificacion`, `UsuarioUltimaModificacion`, `VinculadoID`, `FechaVinculacion`, `Nombres`, `Apellidos`, `Nacionalidad`, `OtraNacionalidad`, `TipoDocumento`, `OtroDocumento`, `NumeroID`, `FechadeExpedicion`, `CiudadExpedicion`, `PaisExpedicion`, `FechaNacimiento`, `CiudadNacimiento`, `PaisNacimiento`, `Genero`, `EstadoCivil`, `DireccionResidencia`, `TelefonoResidencia`, `Celular`, `CiudadResidencia`, `DepartamentoResidencia`, `CorreoElectronico`, `Ocupacion`, `DireccionEmpresa`, `TelefonoEmpresa`, `CiudadEmpresa`, `DepartamentoEmpresa`, `NombreEmpresa`, `Cargo`, `CIIU`, `Activos`, `Pasivos`, `IngresosMensuales`, `EgresosMensuales`, `OtrosIngresos`, `OtrosEgresos`, `DeclaraRenta`, `RazondeIngresos`, `Producto`, `NivelEstudios`, `Beneficiario`, `TipoDocumentoBeneficiario`, `NumeroIDBeneficiario`, `EscolaridadBeneficiario`, `MontoAhorroMensual`, `ValorCadaBono`, `CantidadBonos`, `PrimaJunio`, `PrimaDiciembre`, `DebitoAutomatico`, `FechadeAfiliacion`, `DuenoOportunidad`) VALUES
('2019-07-04 14:06:05',	'',	1,	'2019-07-04 14:06:05.000000',	'test',	'test',	'test',	'test',	'1',	'2',	'12345',	'2019-07-04 14:06:05.000000',	NULL,	'Colombia',	'2019-07-04 14:06:05.000000',	NULL,	NULL,	'Masculino',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1000,	200,	500,	200,	100,	50,	CONV('0', 2, 10) + 0,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	400,	100,	2,	400,	400,	CONV('1', 2, 10) + 0,	'2019-07-04 14:06:05.000000',	NULL);

-- 2019-07-04 16:04:57
